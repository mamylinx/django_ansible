# README #

Ansible playbook for provisioning Django App

### What is this playbook for? ###

* This playbook helps you to provision and to deploy your django application. Assumes that it will use NGINX, GUNICORN and PgSQL
* Version 1.0

### How do I get set up? ###

* Edit `vars.yml` files according to your application.
* Add your server IP and SSH configuration in `hosts` file
* Run ansible-playbook command with `hosts` as inventory file. 
* `provision.yml` playbook will set up your server for the first time and will deploy your application